python-weberror (0.13.1+dfsg-2) UNRELEASED; urgency=medium

  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/changelog: Remove trailing whitespaces
  * d/control: Remove ancient X-Python-Version field
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Ondřej Nový <onovy@debian.org>  Tue, 13 Feb 2018 10:01:35 +0100

python-weberror (0.13.1+dfsg-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed homepage (https)
  * Fixed VCS URL (https)

  [ Andrew Shadura ]
  * New upstream release.

 -- Andrew Shadura <andrewsh@debian.org>  Sat, 30 Apr 2016 19:39:36 +0200

python-weberror (0.10.3+dfsg-1) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Jackson Doak ]
  * Bump debhelper version to 9
  * debian/rules: Use dh

  [ Federico Ceratto ]
  * Add patch to remove embedded minified JS (Closes: #735237)
  * Bump Standards-Version to 3.9.5 (no changes needed)
  * Add Pybuild dependencies
  * Install evaldemo.py example

  [ Andrew Shadura ]
  * Repack the tarball to remove jQuery object files.

 -- Andrew Shadura <andrewsh@debian.org>  Wed, 08 Oct 2014 14:33:11 +0200

python-weberror (0.10.3-1) unstable; urgency=low

  [ Andrey Rahmatullin ]
  * Enable build-time tests

  [ Piotr Ożarowski ]
  * New upstream release
  * Convert to dh_python2
  * Source format changed to 3.0 (quilt)
  * debian/watch updated (to recognize pre release versions)
  * Bump Standards-Version to 3.9.2 (no changes needed)

 -- Piotr Ożarowski <piotr@debian.org>  Mon, 27 Jun 2011 21:45:38 +0200

python-weberror (0.10.2-1) unstable; urgency=low

  * New upstream release
  * Bump Standards-Version to 3.8.4 (no changes needed)

 -- Piotr Ożarowski <piotr@debian.org>  Sun, 14 Feb 2010 22:36:57 +0100

python-weberror (0.10.1-1) unstable; urgency=low

  * New upstream release

 -- Piotr Ożarowski <piotr@debian.org>  Wed, 31 Dec 2008 14:29:47 +0100

python-weberror (0.10-1) unstable; urgency=low

  [ Sandro Tosi ]
  * Switch Vcs-Browser field to viewsvn

  [ Piotr Ożarowski ]
  * New upstream release
  * "python (>=2.5) | python-wsgiref" and "${misc:Depends}" added to Depends

 -- Piotr Ożarowski <piotr@debian.org>  Sun, 21 Dec 2008 20:07:37 +0100

python-weberror (0.9.1-1) unstable; urgency=low

  * New upstream release
  * Bump python-paste's minumum required version to >= 1.7.1

 -- Piotr Ożarowski <piotr@debian.org>  Wed, 29 Oct 2008 23:17:38 +0100

python-weberror (0.9-1) unstable; urgency=low

  [ Carlos Galisteo ]
  * debian/control:
    - Added Homepage field.

  [ Piotr Ożarowski ]
  * New upstream release
  * python-simplejson, python-paste, python-pastedeploy, python-pkg-resources
    and python-pygments added to Depends
  * python, python-all-dev, python-support, and python-setuptools build
    dependencies moved to Build-Depends-Indep
  * dpatch removed from debian/control and debian/rules
  * Vcs-Svn and Vcs-Browser fields added in debian/control
  * Typo fixed in long description (s/functinality/functionality)
  * CHANGELOG file installed as upstream changelog
  * debian/watch file added
  * XS-Python-Version bumped to >=2.4 (xml.dom.minidom)
  * Bump Standards-Version to 3.8.0 (no changes needed)
  * Change Priority to optional
  * Move team name to Maintainer field, add myself to Uploaders

 -- Piotr Ożarowski <piotr@debian.org>  Tue, 22 Jul 2008 23:10:06 +0200

python-weberror (0.8a-3) unstable; urgency=low

  * Added build dependency for dpatch (closes: #483346)

 -- Christoph Haas <haas@debian.org>  Wed, 28 May 2008 19:23:29 +0200

python-weberror (0.8a-2) unstable; urgency=low

  * Added copyright notes for collector.py and jQuery.js

 -- Christoph Haas <haas@debian.org>  Mon, 12 May 2008 12:40:55 +0200

python-weberror (0.8a-1) unstable; urgency=low

  * Initial release (Closes: #476923)

 -- Christoph Haas <haas@debian.org>  Sun, 20 Apr 2008 10:30:40 +0200
